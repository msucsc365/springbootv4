package edu.missouristate.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootv4Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootv4Application.class, args);
	}

}
